/--  ///  /-/  -/-
/--  |||  /-/   |
/--  |||  |    -/-

# Empi
- homepage: http://www.bay12forums.com/smf/index.php?topic=157538
- gem: https://rubygems.org/gems/empi
- repository: https://gitlab.com/rasunadon/empi
- bug tracker: https://gitlab.com/rasunadon/empi/issues
- licence: CC-BY-SA 3.0, Detros
- email: rasunadon@seznam.cz

_Empi_ (full name _Empi: Ruby Edition_) is a turn-based wargame with hotseat
multiplayer. You take command of one faction and try to use various units to
win over the other one.


## Status
There are several substantial imperfections due to alpha development state
of Empi:
- "various units" currently means just two movable pieces
- units damaged in combat are outright destroyed
- no saving or loading of active map

More planned features and other recognized imperfections can be found in
[GitLab issues](https://gitlab.com/rasunadon/empi/-/issues).


## Inspiration
While learning Ruby I have found there are hardly any strategic games available
as gems so I picked this genre for my project. The game is inspired by Classic
Empire, a wargame originally from 1977.
[VMS-Empire](https://gitlab.com/esr/vms-empire), one of its many ports,
is available also for modern platforms.


## Installation
1. Get _ruby_ – Some of you may already have ruby included on your computers.
Others may use their favourite package management system or check the
[documentation](https://www.ruby-lang.org/en/documentation/installation/)
for other ways.
2. Get _gosu_ – Run `gem install gosu`.
Check [additional requirements](https://github.com/gosu/gosu/wiki/Getting-Started-on-Linux)
if you are on Linux.
3. Get _Empi_ – Here you can pick from two options:
    - either download this repository, navigate to its folder and run
    `ruby empi.rb`
    - or obtain empi gem with `gem install empi`, create small runfile
    somewhere, call it `empitest.rb`, navigate to its location and run it
    with `ruby empitest.rb`:

          require 'empi'
          Empi::GameWindow.initialize


## Controls
_Empi_ is run from terminal and controlled via a keyboard when its main window
is focused. The main window shows infopane and a view of the battlefield.
The infopane consists of turn, faction and score counters, freeroam marker and
the status of the currently selected unit. Additional info is printed out to
the original terminal window. Using QWERTY keyboard layout is advised.

                      QWE                    ↑                     N7 N8 N9
    movement          A D   or   arrow keys ← →   or   numpad keys N4    N6
                      ZXC                    ↓                     N1 N2 N3

    show main menu    Esc
    get unit info     Enter   or   numpad key N5
    end turn          . (full stop)

    show help         H
    switch freeroam   J   or   numpad key N0
    previous unit     K   or   numpad key NMINUS
    next unit         L   or   numpad key NPLUS

    set functions     S – sentry
                      B – build
                      N – none



In menus, press key listed at each option to select it. Esc key can be used to
return to the previous screen.


## Units
Note some of these stats and abilities, especially armour and moves, are
probably going to be later changed due to balancing reasons.

### Stats
| Unit | Armour | Moves | Cargo cap | Price |
| ---- |:------:|:-----:|:---------:|:-----:|
| Army | 1      | 5     | 0         | 3     |
| Ship | 1      | 2     | 3         | 5     |
| Town | 1      | 0     | 10        | --    |

- *Armour*: how many hits can this unit withstand before being destroyed
- *Moves*: how many tiles can this unit move per turn
- *Cargo capacity*: how many other units can this unit hold at once
- *Price*: how many turns it takes to produce this unit

### Abilities
| Unit | Ground | Sea | Capture | Be captured | Build | Be built |
| ---- |:------:|:---:|:-------:|:-----------:|:-----:|:--------:|
| Army | Y      | N   | Y       | N           | N     | Y        |
| Ship | N      | Y   | N       | N           | N     | Y        |
| Town | --     | --  | N       | Y           | Y     | N        |

- *Ground*: can this unit safely move to ground tile?
- *Sea*: can this unit safely move to sea tile?
- *Capture*: is this unit able to capture other units?
- *Be captured*: can this unit be captured?
- *Build*: can this unit produce other units?
- *Be built*: can this unit be produced?


## Rules
- Each movable unit has limited amount of moves per turn. Each move, attack or
capture spends one of these points. All remaining moves of given unit are spent
when it gets loaded into transporter.
- Movable unit which moves to an unsuitable terrain gets destroyed.
- Only armies can capture.
- When captured, towns lose all parts and reset to their default project.
New owner is then offered to select a different project.
- Capturable units (towns) don't fight themselves and use their cargo as
defenders instead. Transporters (ships) can't use their cargo in combat so when
they are destroyed all their cargo is lost with them.
- Transporters can be loaded into other transporters but only those with bigger
cargo capacity than their own. Such loaded transporters still take only one
cargo slot in the big transporter, be they empty or full themselves.


## Functions and cursor
Each unit has one function that changes how the unit behaves. Functions are
evaluated at the start of turn of the faction given unit belongs to.
- none: ask for commands if you have unspent moves
- sentry: do not ask for commands until there are not-allied units nearby at the
start of your turn, then wake up (set function to none)
- build: produce new unit every few turns, according to the selected project

Cursor jumps to the next unit waiting for commands when current one uses its
last move. If you want to give commands to some other unit first (e.g. move
transporter closer so it can be boarded) or wake up sentrying unit, switch to
freeroam mode and move cursor to given unit. You can set functions while
in freeroam. Switching out of freeroam when one of your units still waiting
for commands is selected will select that unit, switching out of it elsewhere
will make the cursor jump back to the last unit it was locked to. If there are
no units waiting for commands left cursor switches itself to freeroam mode.

In locked mode, you can skip to the next (or previous) unit waiting for
commands, wherever it is. On the other hand, in freeroam mode, you can cycle to
the next (or previous) unit at the current tile, whatever state it is in. Search
for eligible units goes up-to-down, then left-to-right; units at tile are sorted
by recursive cargo order ‒ transported units are recursively put behind their
transporter.

# Victory
There is currently only one victory type, _Annihilation_. To satisfy it,
destroy or capture all enemy units. As this victory condition is checked only
at the start of faction turn remember to end your turn once there are no enemy
units left.

Win screen shows who has won, which type of victory was achieved and what were
the turn and score counters at that time. One can resume won game from win
screen and continue playing it or return to the main menu. Until the next map is
started, one can also return to win screen from the main menu.


# Other documentation
- CHANGELOG: list of what has been added/changed/fixed/removed in given version
- CONTRIBUTING: gitting standards and how to contribute to the project
