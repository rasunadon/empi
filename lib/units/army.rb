require_relative './unit'

class Army < Unit
  @name = 'army'
  @map_symbol = 'A'
  @price = 3
  @value = 5

  def initialize(x, y, faction, map, infopane, built_by = nil)
    super
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/army.png')

    @armour_left = @armour_max = 3
    @moves_max = 5
  end

  def can_ride?
    true
  end

  def can_capture?
    true
  end
end
