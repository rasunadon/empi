class UnitFunction
  attr_accessor :func

  def initialize(function = FUNCNONE, unit)
    @func = function
    @unit = unit
  end

  # Set function part of long info string of unit
  def info
    case @func
    when FUNCNONE
      "no function"
    when FUNCBUILD
      unless @unit.project
        abort("UnitFunction.info(): No project set (" + @unit.to_s + ")")
      end
      "building #{@unit.project.name} (#{@unit.build_info})"
    when FUNCSENTRY
      "sentrying"
    end
  end

  def func!(map, infopane)
    case @func
    # Build given unit
    when FUNCBUILD
      unless @unit.project
        abort("UnitFunction.func!(): No project set (" + @unit.to_s + ")")
      end

      @unit.parts_built += 1

      unless @unit.parts_built >= @unit.parts_needed # just == should be enough
        ret = " built next part of #{@unit.project.name} (#{@unit.build_info} done)"
      else
        ret = " completed building of #{@unit.project.name}"
        @unit.parts_built = 0
        @unit.project.new(@unit.x, @unit.y, @unit.faction, map, infopane, @unit)
      end

    # Wake when enemies are nearby (checking just map units is enough)
    when FUNCSENTRY
      units_around = map.all_units(0,0).select { |uu|
        (uu.x - @unit.x).abs <= 1 &&
        (uu.y - @unit.y).abs <= 1 &&
        uu.faction != @unit.faction
      }
      if units_around.size > 0
        ret = " woke up"
        @func = FUNCNONE
      else
        ret = " was on sentry duty"
      end
    end

    unless ret
      abort("UnitFunction.func!(): Functionable unit didn't function (" + @unit.to_s + ")")
    end

    ret
  end
end
