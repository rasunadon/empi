require_relative './unit'

class Ship < Unit
  @name = 'ship'
  @map_symbol = 'S'
  @price = 5
  @value = 10

  def initialize(x, y, faction, map, infopane, built_by = nil)
    super
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/ship.png')

    @armour_left = @armour_max = 1
    @moves_max = 2
    @cargo_max = 3
  end

  def can_sail?
    true
  end
end
