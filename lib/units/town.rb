require_relative './army'
require_relative './ship'
require_relative './unit'

require_relative './../game_states/game_state'
require_relative './../game_states/build_state'

class Town < Unit
  @name = 'town'
  @map_symbol = 'T'
  @value = 20

  attr_accessor :project, :parts_built, :parts_needed

  def initialize(x, y, faction, map, infopane)
    super
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/town.png')

    @armour_left = @armour_max = 1
    @moves_max = 0
    @cargo_max = 10

    @starting_project = Army unless @faction == 0 # used once at the game start
    @default_project = Army # used after capture
    @project = nil
    @parts_built = 0
    @parts_needed = 0

    set_function!(FUNCBUILD, @faction) unless @faction == 0
  end

  def can_build?
    true
  end

  def can_be_captured?
    true
  end

  # Tell the state of current build project
  def build_info
    "#{@parts_built}/#{@parts_needed}"
  end

  # Process capture targeted at this town and reset build process
  def capture!(by_whom)
    super

    # Reset build process
    # 1) remove old project so that it is not disclosed
    # 2) set default project so that there is always some set (remove old parts)
    # 3) offer change of the project to the new owner
    @function.func = FUNCNONE
    @project = nil
    set_project!(@default_project)
    set_function!(FUNCBUILD, @faction)
  end

  # Set desired function and possibly also project
  def set_function!(func, commanding_faction)
    super

    if @faction != 0 and commanding_faction == @faction and func == FUNCBUILD
      # Set starting project once or ask player about next project
      if @starting_project
         set_project!(@starting_project)
         @starting_project = nil
      else
        GameState.switch!(BuildState.instance)
        BuildState.instance.unit = self
      end
    end
  end

  # Set desired project
  def set_project!(desired_project)
    unless price_list.key?(desired_project)
      abort("Town.set_project!(): Unknown project (#{desired_project})")
    end
    @parts_needed = price_list[desired_project]

    # Compare new setting with the old one
    if desired_project == @project
      puts PROMPT + to_s + ": project has already been set to #{@project.name} (#{build_info} done)"
    else
      previous_project = @project
      @project = desired_project
      lost_parts = @parts_built
      @parts_built = 0

      new_project_set_text = PROMPT + to_s + ": project set to #{@project.name} (#{build_info} done)"
      if previous_project and lost_parts > 0 # parts were lost but not due to capture
        puts new_project_set_text + ", losing #{lost_parts} " +
          "part#{ 's' unless lost_parts == 1 } of #{previous_project.name}"
      else
        puts new_project_set_text
      end
    end
  end

  # Load all prices
  def price_list
    prices = Hash[
      [Army, Ship].collect { |ii| [ii, ii.price] } # TODO all subclasses of Unit which can_be_built?
    ]
  end
end
