require_relative './../game_states/game_state'
require_relative './../game_states/welcome_state'

TILESIZE = 50
MAPX = 10
MAPY = 10
LINE_HEIGHT = 20

BUTTON_PROCESSED = -1

# Main window
class GameWindow < Gosu::Window
  attr_accessor :state

  def initialize(version,
                 width = (MAPX + 1) * TILESIZE, \
                 height = (MAPY + 2) * TILESIZE, \
                 fullscreen = false)
    super(width, height, fullscreen)

    # Set version name
    self.caption = "Empi: Ruby Edition #{version}"
    WelcomeState.instance.version = version

    # Activate first state
    $window = self
    GameState.switch!(WelcomeState.instance)
  end

  # Catch the released button
  def button_up(key)
    @button = key
  end

  # Process given button according to current state
  def update
    # No (new) keys
    unless @button == BUTTON_PROCESSED || @button.nil?
      @state.update(@button)
    end
  end

  # Draw only after some button was released and in the start
  def needs_redraw?
    @button != BUTTON_PROCESSED
  end

  # Draw according to current state
  def draw
    @button = BUTTON_PROCESSED # draw just once after each button release
    unless $window.state.nil?
      @state.draw
    end
  end
end
