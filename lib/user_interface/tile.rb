require_relative './../game_states/set_state'

TILE_SEA = 0
TILE_GROUND = 1

SYMBOL_SEA = '.'
SYMBOL_GROUND = '#'

# Terrain cell, can include a unit
class Tile
  attr_accessor :terrain, :infopane, :unit

  def initialize(x, y, loaded_symbol, infopane)
    dir_path = File.dirname(__FILE__)

    @x = x
    @y = y
    @infopane = infopane

    case(loaded_symbol)
    when SYMBOL_SEA then
      @terrain = TILE_SEA
      @image = Gosu::Image.new(dir_path + '/../../media/sea.png')
    when SYMBOL_GROUND then
      @terrain = TILE_GROUND
      @image = Gosu::Image.new(dir_path + '/../../media/ground.png')
    else
      abort("Tile.initialize(): Unknown terrain symbol (#{loaded_symbol})")
    end
  end

  # Draw terrain, unit and axes parts
  def draw
    # 1) terrain
    @image.draw(@x * TILESIZE, (@y + 1) * TILESIZE, ZTILE)

    # 2) unit
    if @unit
      @unit.draw
    end

    # 3) axes
    near_tick = 0
    mid_tick = (TILESIZE - LINE_HEIGHT) / 2
    far_tick = TILESIZE - LINE_HEIGHT

    draw_axis_tick(@x, @y, 0, :center, near_tick) if SetState.instance.settings['axes']['top'] # TODO 0 -> viewport.y
    draw_axis_tick(@x, @y, MAPY, :center, far_tick) if SetState.instance.settings['axes']['bottom']
    draw_axis_tick(@y, @x, 0, :left, mid_tick) if SetState.instance.settings['axes']['left'] # TODO 0 -> viewport.x
    draw_axis_tick(@y, @x, MAPX, :right, mid_tick) if SetState.instance.settings['axes']['right']
  end

  # Draw one tick of axis for appropriate tiles
  def draw_axis_tick(tick_value, tile_coord, desired_coord, x_alignment, y_subcoord)
    if tile_coord == desired_coord
      tick = Gosu::Image.from_text(tick_value, LINE_HEIGHT, {:width => TILESIZE, :align => x_alignment})
      tick.draw(
        @x * TILESIZE,
        (@y + 1) * TILESIZE + y_subcoord,
        ZTEXT
      )
      # ^^ TODO substract desired_coord of viewport from @x and @y here?
    end
  end
end
