require_relative './tile'

require_relative './../units/army'
require_relative './../units/ship'
require_relative './../units/town'

MAX_CARGO_LEVEL = 10

# Grid of terrain cells
class Map
  attr_accessor :name, :mapx, :mapy, :infopane

  def initialize(file, infopane)
    dir_path = File.dirname(__FILE__)

    @infopane = infopane
    @known_unit_types = Hash[
      [Army, Ship, Town].collect { |ii| [ii.map_symbol, ii] }
    ]

    load_map!(dir_path + "/../../save/#{file}.esf")
  end

  # Load map from file
  def load_map!(file_path)
    unless File.file?(file_path)
      abort("Map.load_map!(): File not found (#{file_path})")
    end

    input = File.open(file_path, 'r')
    unit_count = load_head!(input.gets)

    # Load tiles
    @tiles = []
    0.upto(@mapy - 1) { |rr|
      @tiles[rr] = []
      map_row = input.gets
      0.upto(@mapx - 1) { |cc|
        @tiles[rr][cc] = Tile.new(cc, rr, map_row[cc], @infopane)
      }
    }

    # Load units
    unit_count.times { load_unit!(input.gets) }

    puts("Save loaded: #{@name} with #{unit_count} units")
    input.close
  end

  # Load core info from given head row of file
  # Return number of units to be loaded
  def load_head!(row)
    head = []
    size = []

    head = row.split(' ')
    size = head[1].split('x')

    @name = head[0]
    @mapx = size[0].to_i
    @mapy = size[1].to_i

    head[2].to_i # unit_count
  end

  # Load one unit from given row
  def load_unit!(row)
    unit = []
    coords = []

    unit = row.split(' ')

    # Check coordinates
    coords = unit[2].split('-')
    coords_x = coords[0].to_i
    coords_y = coords[1].to_i
    if (coords_x < 0 || coords_x >= @mapx ||
        coords_y < 0 || coords_y >= @mapy)
      abort("Map.load_unit!(): Unit out of map borders (#{coords_x}-#{coords_y})")
    end

    # Check faction
    fac = unit[1].to_i
    if(fac < 0 || fac > FACTIONS)
      abort("Map.load_unit!(): Bad faction id (#{fac})")
    end

    # Create unit
    unit_type = unit[0]
    if @known_unit_types.include?(unit_type)
      @known_unit_types[unit_type].new(coords_x, coords_y, fac, self, @infopane)
    else
      abort("Map.load_unit!(): Unknown unit type symbol (#{unit_type})")
    end
  end

  # Draw all tiles
  def draw_tiles
    all_tiles.each { |tt| tt.draw}
  end

  # Getter for tile at given coordinates
  def tile(cc, rr)
    @tiles[rr][cc]
  end

  # Return all tiles
  def all_tiles
    ret = []
    ii = 0
    0.upto(MAPY) { |rr|
      0.upto(MAPX) { |cc|
        ret[ii] = @tiles[rr][cc]
        ii += 1
      }
    }
    ret
  end

  # Getter for unit at given coordinates
  def get_unit(cc, rr)
    @tiles[rr][cc].unit
  end

  # Setter for unit at given coordinates
  def set_unit(cc, rr, uu)
    @tiles[rr][cc].unit = uu
  end

  # Return all units with cargo level in given range
  # CL0 = map units, CL1 = units in map units, CL2 = units in CL1 units...
  # all_units() -> all units, all_units(0, 0) -> only map units
  def all_units(min_cargo_level = 0, max_cargo_level = MAX_CARGO_LEVEL)
    ret = []

    unless min_cargo_level.between?(0, MAX_CARGO_LEVEL) and
           max_cargo_level.between?(0, MAX_CARGO_LEVEL)
      abort("Map.all_units(): Desired cargo levels need to be " \
            "from 0 to #{MAX_CARGO_LEVEL}")
    end
    unless min_cargo_level <= max_cargo_level
      abort("Map.all_units(): Min cargo level (#{min_cargo_level}) is higher " \
            "than max cargo level (#{max_cargo_level})")
    end

    0.upto(MAPY) { |rr|
      0.upto(MAPX) { |cc|
        ret += all_units_from_tile(cc, rr)
      }
    }

    ret
  end

  # Return all units from given tile with cargo level in given range
  def all_units_from_tile(cc, rr,
                          min_cargo_level = 0,
                          max_cargo_level = MAX_CARGO_LEVEL)
    ret = []

    uu = get_unit(cc, rr)
    if uu
      ret += add_unit_to_list(uu, 0, min_cargo_level, max_cargo_level)
    end

    ret
  end

  # Recursively add units to the list
  def add_unit_to_list(unit, cargo_level = 0,
                       min_cargo_level = 0,
                       max_cargo_level = MAX_CARGO_LEVEL)
    ret = []

    # This unit
    if cargo_level >= min_cargo_level
      ret.append(unit)
    end

    # Transported units
    cargo_level += 1
    if cargo_level <= max_cargo_level
      unit.cargo.each { |uu|
        ret += add_unit_to_list(uu, cargo_level, min_cargo_level, max_cargo_level)
      }
    end

    ret
  end
end
