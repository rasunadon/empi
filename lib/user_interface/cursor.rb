# Selected tile, selected unit
class Cursor
  attr_accessor :freeroam, :info_stopped

  def initialize(x, y, map, infopane)
    dir_path = File.dirname(__FILE__)

    @x = x
    @y = y
    @map = map
    @infopane = infopane

    @image = Gosu::Image.new(dir_path + '/../../media/cursor.png')
    @freeroam = false
    @info_stopped = false

    # Give to Infopane link to yourself (for freeroam marker)
    if !@infopane
      abort("Cursor.initialize(): Infopane is not set")
    end
    @infopane.cursor = self
  end

  def update(button)
    case button
    # Cardinal directions
    when Gosu::KB_LEFT, Gosu::KB_A, Gosu::KB_NUMPAD_4 then
      move!(-1, 0) unless @x <= 0
    when Gosu::KB_RIGHT, Gosu::KB_D, Gosu::KB_NUMPAD_6 then
      move!(1, 0) unless @x >= MAPX
    when Gosu::KB_UP, Gosu::KB_W, Gosu::KB_NUMPAD_8 then
      move!(0, -1) unless @y <= 0
    when Gosu::KB_DOWN, Gosu::KB_X, Gosu::KB_NUMPAD_2 then
      move!(0, 1) unless @y >= MAPY

    # Intercardinal directions
    when Gosu::KB_Q, Gosu::KB_NUMPAD_7 then
      move!(-1, -1) unless @x <= 0 || @y <= 0
    when Gosu::KB_E, Gosu::KB_NUMPAD_9 then
      move!(1, -1) unless @x >= MAPX || @y <= 0
    when Gosu::KB_Z, Gosu::KB_NUMPAD_1 then
      move!(-1, 1) unless @x <= 0 || @y >= MAPY
    when Gosu::KB_C, Gosu::KB_NUMPAD_3 then
      move!(1, 1) unless @x >= MAPX || @y >= MAPY

    # Functions
    when Gosu::KB_S then
      set_function_to_unit(FUNCSENTRY)
    when Gosu::KB_B then
      set_function_to_unit(FUNCBUILD)
    when Gosu::KB_N then
      set_function_to_unit(FUNCNONE)

    # The rest
    when Gosu::KB_J, Gosu::KB_NUMPAD_0 then
      switch_freeroam!
    when Gosu::KB_K, Gosu::KB_NUMPAD_MINUS then
      select_other_unit!(false)
    when Gosu::KB_L, Gosu::KB_NUMPAD_PLUS then
      select_other_unit!(true)
    when Gosu::KB_RETURN, Gosu::KB_NUMPAD_5 then
      info
    end

    # If in locked mode, stay at current/jump to next movable unit
    to_next_unit! unless @freeroam
  end

  def draw
    @image.draw(@x * TILESIZE, (@y + 1) * TILESIZE, ZCURSOR)
  end

  # Move to coordinates of map unit the cursor is locked to
  def warp_to_locked!()
    @x = @locked_unit.x
    @y = @locked_unit.y
    @selected_unit = @locked_unit
  end

  # Move by given change of coordinates
  def move!(xx, yy)
    if freeroam
      @x += xx
      @y += yy
      @selected_unit = @map.get_unit(@x, @y)
      # TODO show some basic tile info in infopane
    else
      check_unit

      # Move the unit first
      @locked_unit.x += xx
      @locked_unit.y += yy
      @locked_unit.check_movement(@x, @y) # cursor coordinates work like old_x, old_y

      # Is the unit still alive?
      if @locked_unit.armour_left > 0
        warp_to_locked! # whether it moved or not
      else
        # It got destroyed so clear last links then so that (object of)
        # given unit can be truly destroyed
        @selected_unit = nil
        @locked_unit = nil
      end
    end
  end

  # Tries to set function <func> to selected unit
  def set_function_to_unit(func)
    check_unit

    if @selected_unit
      @selected_unit.set_function!(func, @infopane.faction)

      # Update infopane with the new (possibly changed) state
      # (visible only in freeroam mode as in locked one the infopane is
      # overwritten as cursor either jumps away or switches to freeroam mode)
      @infopane.text = @selected_unit.info
    else
      puts "no unit to set that function to (at #{@x}-#{@y})"
    end
  end

  # Reset to locked mode
  def reset!
    @freeroam = true
    switch_freeroam!
    @selected_unit = nil
    @locked_unit = nil
    to_next_unit!
  end

  # Find next unit which is still waiting for commands and lock to it
  # (selected -> last locked to -> next waiting) or switch (back) to freeroaming
  def to_next_unit!
    if @selected_unit and @selected_unit.is_waiting_for_commands?
      # Lock to such unit (though it may have already been locked)
      @locked_unit = @selected_unit
    else
      unless @locked_unit and @locked_unit.is_waiting_for_commands?
        waiting = @map.all_units.select { |uu| uu.is_waiting_for_commands? }

        # Are there still some units of active faction waiting for commands?
        if waiting.size <= 0 # == would be enough
          puts 'all movable units without functions moved'
          switch_freeroam!
          return
        end

        # Pick the first waiting unit (leftmost of topmosts)
        @locked_unit = waiting[0]
      end
    end

    warp_to_locked! # stay at old or go to new
    info unless @info_stopped # due to switching out of the play game state
  end

  # Skip or cycle units, depending on cursor mode
  def select_other_unit!(search_forward)
    unless @freeroam
      skip_to_other_unit!(search_forward)
    else
      cycle_to_other_unit!(search_forward)
    end
  end

  # Skip from selected unit waiting for commands to the next/previous waiting one
  def skip_to_other_unit!(search_forward = true)
    if @freeroam
      abort("Cursor.skip_to_other_unit!(): " \
            "skipping to #{ search_forward ? 'next' : 'previous'} " \
            "unit works only in locked mode, not in freeroam one")
    end

    waiting = @map.all_units.select { |uu| uu.is_waiting_for_commands? }

    # Pick the previous/next waiting unit, relative to the current one
    # (picking the previous of the first results in index -1 which is the last
    # so there it is fine but taking the next of the last would go out of range)
    current_index = waiting.index(@selected_unit)
    other_index = search_forward ? current_index + 1 : current_index - 1
    other_index = 0 if other_index >= waiting.size # == would be enough

    @locked_unit = waiting[other_index]
    warp_to_locked! # so that to_next_unit!() doesn't stay at @selected_unit
  end

  # Cycle from selected unit to the next/previous local one
  def cycle_to_other_unit!(search_forward = true)
    unless @freeroam
      abort("Cursor.cycle_to_other_unit!():" \
            "cycling to #{ search_forward ? 'next' : 'previous'} " \
            "unit works only in freeroam mode, not in locked one")
    end

    unless @selected_unit
      @infopane.text = ''
      puts "no units to cycle through (at #{@x}-#{@y})"
      return
    end

    local_pile = @map.all_units_from_tile(@x, @y)

    # Pick the previous/next local unit, relative to the current one
    # (picking the previous of the first results in index -1 which is the last
    # so there it is fine but taking the next of the last would go out of range)
    current_index = local_pile.index(@selected_unit)
    other_index = search_forward ? current_index + 1 : current_index - 1
    other_index = 0 if other_index >= local_pile.size # == would be enough

    @selected_unit = local_pile[other_index]
    info
  end

  # When cursor is in locked mode there needs to be unit it is locked to
  # When cursor is in freeroam mode the locked unit should be kept
  def check_unit
    if !@freeroam and !@locked_unit
      abort("Cursor.check_unit(): Cursor is in locked mode " \
            "but there is no unit it is locked to (at #{@x}-#{@y})")
    end
  end

  # Switch between being attached to unit and being able to freeroam
  def switch_freeroam!
    if freeroam
      @infopane.text = 'freeroam disabled'
      puts 'freeroam disabled'
      @freeroam = false
    else
      @infopane.text = 'freeroam enabled'
      puts 'freeroam enabled'
      @freeroam = true
    end
  end

  # Find some info about units on the current tile
  def info
    check_unit

    if @selected_unit
      @infopane.text = @selected_unit.info
      puts @selected_unit.info

      if @selected_unit.is_transporting?
        @selected_unit.cargo.each { |uu| puts '- cargo: ' + uu.info }
      end
    else
      @infopane.text = ''
      puts "no unit to show info of (at #{@x}-#{@y})"
    end
  end
end
