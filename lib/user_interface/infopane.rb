# Score, turn and event texts
class Infopane
  attr_reader :faction, :turn, :score
  attr_writer :cursor, :text

  def initialize
    @turn = 0
    @faction = FACTIONS # so that FAC1 will be then the first active faction
    @score = [0, 0]
    @text = 'ready'
  end

  def update
  end

  def draw
    freeroam_text = ""
    freeroam_text = " (freeroam)" if (@cursor and @cursor.freeroam)

    state = Gosu::Image.from_text(turnscore + freeroam_text, LINE_HEIGHT)
    state.draw(XTEXT, YTEXT, ZTEXT)

    text = Gosu::Image.from_text("#{@text}", LINE_HEIGHT)
    text.draw(XTEXT, YTEXT + (TILESIZE / 2), ZTEXT)
  end

  def turnscore
    "Turn: #{@turn}, " \
    "Faction: FAC#{@faction}, " \
    "Score: #{@score[0]} - #{@score[1]}"
  end

  # Increment faction counter (modulo count of factions)
  def next_faction!
    @faction += 1
    @faction = 1 if @faction > FACTIONS # marks the end of turn
  end

  # Increment turn counter
  def next_turn!
    @turn += 1
  end

  def add_score(to_whom, how_much)
    @score[to_whom] += how_much
  end
end
