require 'singleton'
require 'yaml'

require_relative './game_state'
require_relative './welcome_state'

# Game state of settings screen
class SetState < GameState
  include Singleton

  # TODO move out?
  attr_reader :settings

  # What to do just after state gets activated
  #def after_start
  #end

  # What to do just before state gets deactivated
  def before_end
    dir_path = File.dirname(__FILE__)
    save_settings(dir_path + "/../../data/config.yaml")
  end

  def initialize
    super

    dir_path = File.dirname(__FILE__)
    load_settings!(dir_path + "/../../data/config.yaml")
  end

  # Process given button
  def update(button)
    case(button)
    when Gosu::KB_1, Gosu::KB_2, Gosu::KB_3, Gosu::KB_4 then
      change_setting!(button)
    when Gosu::KB_ESCAPE then
      GameState.switch!(WelcomeState.instance)
    end
  end

  def draw
    header_text = '
        /--  ///  /-/  -/-
        /--  |||  /-/   |
        /--  |||  |    -/-'
    options_text = "
        1 – show top axis: #{@settings['axes']['top']}
        2 – show bottom axis: #{@settings['axes']['bottom']}
        3 – show left axis: #{@settings['axes']['left']}
        4 – show right axis: #{@settings['axes']['right']}\n
        Esc – return to menu"

    menu = Gosu::Image.from_text(
      header_text + "\n\n\n\n\n" + options_text, LINE_HEIGHT)

    menu.draw((3*TILESIZE) + XTEXT, (2*TILESIZE) + YTEXT, ZTEXT)
  end

  # Change value of one setting
  def change_setting!(button)
    changed = {
      Gosu::KB_1 => ['axes', 'top'],
      Gosu::KB_2 => ['axes', 'bottom'],
      Gosu::KB_3 => ['axes', 'left'],
      Gosu::KB_4 => ['axes', 'right'],
    }[button]

    old_value = @settings[changed[0]][changed[1]]
    @settings[changed[0]][changed[1]] = !@settings[changed[0]][changed[1]] # TODO other than boolean types?
    puts "setting \"#{changed[0]} -> #{changed[1]}\" changed from #{old_value} " \
         "to #{@settings[changed[0]][changed[1]]}"
  end

  # Load settings from file
  def load_settings!(file_path)
    unless File.file?(file_path)
      abort("SetState.load_settings!(): File not found (#{file_path})")
    end

    @settings = YAML.load(File.read(file_path))
    puts "settings loaded"
  end

  # Save settings to file
  def save_settings(file_path)
    unless File.file?(file_path)
      abort("SetState.save_settings(): File not found (#{file_path})")
    end

    File.open(file_path, "w") { |file| file.write(@settings.to_yaml) }
    puts "settings saved"
  end
end
