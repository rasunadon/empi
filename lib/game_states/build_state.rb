require 'singleton'

require_relative './game_state'
require_relative './play_state'

require_relative './../units/army'
require_relative './../units/ship'

# Game state of selection of the build project
class BuildState < GameState
  include Singleton

  attr_accessor :unit

  # What to do just after state gets activated
  #def after_start
  #end

  # What to do just before state gets deactivated
  #def before_end
  #end

  # Process given button
  def update(button)
    newly_selected = nil

    case(button)
    # Cancelled change of project = return
    when Gosu::KB_ESCAPE then
      puts PROMPT + @unit.to_s + ": cancelling, nothing changed"
      GameState.switch!(PlayState.instance)
    # Paused production = reset function and return
    when Gosu::KB_0, Gosu::KB_N then
      puts PROMPT + @unit.to_s + ": production paused, resetting function"
      @unit.set_function!(FUNCNONE, @unit.faction)
      GameState.switch!(PlayState.instance)
    # Selected new project = set it and return
    when Gosu::KB_1, Gosu::KB_A then
      newly_selected = Army
    when Gosu::KB_2, Gosu::KB_S then
      newly_selected = Ship
    end

    # Did we get any proper answer?
    if newly_selected
      @unit.set_project!(newly_selected)
      GameState.switch!(PlayState.instance)
    end
  end

  def draw
    # Combine parts according to if there already is some project set
    unit_text = "Built so far in " + @unit.to_s + ":"
    project_text = "
        #{@unit.parts_built} " +
        "part#{ 's' unless @unit.parts_built == 1 } of " +
        @unit.project.name + " (" + @unit.build_info + ")"
    options_text = "Select the project:\n
        1, A – Army (#{ @unit.price_list[Army] } turns)
        2, S – Ship (#{ @unit.price_list[Ship] } turns)\n
        0, N – pause production
        Esc – cancel change of project"

    build_project = Gosu::Image.from_text(
      unit_text + "\n" + project_text + "\n\n" + options_text, LINE_HEIGHT)
    build_project.draw((2*TILESIZE) + XTEXT, (2*TILESIZE) + YTEXT, ZTEXT)
  end
end
