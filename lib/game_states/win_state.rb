require 'singleton'

require_relative './game_state'
require_relative './play_state'
require_relative './welcome_state'

# Game state of summary of won game
class WinState < GameState
  include Singleton

  attr_reader :faction

  # What to do just after state gets activated
  #def after_start
  #end

  # What to do just before state gets deactivated
  #def before_end
  #end

  # Process given button
  def update(button)
    case(button)
    when Gosu::KB_ESCAPE then
      GameState.switch!(WelcomeState.instance)
    when Gosu::KB_RETURN then
      GameState.switch!(PlayState.instance)
    end
  end

  def draw
    winner_text = "VICTORY FOR FACTION #{@faction}"
    summary_text = "Summary:
        victory type: #{@type}
        won on turn: #{@turn}
        ending score: #{@score}"
    options_text = 'Do you want to continue playing?
        Enter – resume
        Esc – return to menu'

    win_summary = Gosu::Image.from_text(
      winner_text + "\n\n\n" + summary_text + "\n\n" + options_text, LINE_HEIGHT)
    win_summary.draw((2*TILESIZE) + XTEXT, (2*TILESIZE) + YTEXT, ZTEXT)
  end

  def set_victory!(faction, type, turn, score)
    @faction = faction
    @type = type
    @turn = turn
    @score = score.clone # separate object is neeeded

    GameState.switch!(WinState.instance)
  end

   def unset_victory!()
    @faction = nil
    @type = nil
    @turn = nil
    @score = nil
  end
end
