require 'singleton'

require_relative './game_state'
require_relative './welcome_state'

# Game state of starting a new scenario
class StartNewState < GameState
  include Singleton

  # What to do just after state gets activated
  #def after_start
  #end

  # What to do just before state gets deactivated
  #def before_end
  #end

  # Process given button
  def update(button)
    case(button)
    when Gosu::KB_Y then
      GameState.switch!(PlayState.instance) # new map got set in WelcomeState
    when Gosu::KB_N, Gosu::KB_ESCAPE then
      PlayState.instance.desired_new_map = nil
      GameState.switch!(WelcomeState.instance)
    end
  end

  def draw
     confirmation = Gosu::Image.from_text(
       "Are you sure you want to start\n" \
       "     a new scenario? Y/N", LINE_HEIGHT)
     confirmation.draw((3*TILESIZE) + XTEXT, (4*TILESIZE) + YTEXT, ZTEXT)
  end
end
