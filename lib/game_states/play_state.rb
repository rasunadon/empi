require 'singleton'

require_relative './game_state'
require_relative './welcome_state'
require_relative './win_state'

require_relative './../user_interface/cursor'
require_relative './../user_interface/infopane'
require_relative './../user_interface/map'

XTEXT = 5
YTEXT = 5
ZTILE = 0
ZUNIT = 1
ZTEXT = 2
ZCURSOR = 3

FACTIONS = 2
COLOUR = [0xff_ffffff, 0xff_ff3300, 0xff_ffcc00]

FUNCNONE = 'none'
FUNCSENTRY = 'sentry'
FUNCBUILD = 'build'

# Game state of main gameplay
class PlayState < GameState
  include Singleton

  attr_reader :map
  attr_writer :desired_new_map

  # Load new scenario or resume game
  def after_start
    if @desired_new_map
      WinState.instance.unset_victory!
      load_new_map!
      help # show help after each loading of a new map
      @desired_new_map = nil

      next_faction_turn! # so FAC1 will be the first active faction, with announcement
    else
      # Make sure everything is set up
      unless @infopane and @map and @cursor
        abort("PlayState.after_start(): Map parts are not properly loaded
          (#{@infopane}, #{@map}, #{@cursor})")
      end

      @cursor.info_stopped = false

      # Remind player the current status
      if @cursor.freeroam
        @infopane.text = 'freeroam is enabled'
        puts 'freeroam is enabled'
      else
        @cursor.info
      end
    end
  end

  # What to do just before state gets deactivated
  def before_end
    @cursor.info_stopped = true
  end

  # Process given button or send it to cursor
  def update(button)
    case(button)
    when Gosu::KB_ESCAPE then
      unless WinState.instance.faction
        GameState.switch!(WelcomeState.instance)
      else
        GameState.switch!(WinState.instance)
      end
    when Gosu::KB_PERIOD then
      next_faction_turn!
    when Gosu::KB_H then
      help
    else
      @cursor.update(button)
    end
  end

  # Draw all parts of main window
  def draw
    @map.draw_tiles
    @cursor.draw
    @infopane.draw
  end

  # Load the desired map
  def load_new_map!
    puts("=============\n" \
         "=============\n" \
         "Loading save: #{@desired_new_map}")
    @infopane = Infopane.new
    @map = Map.new(@desired_new_map, @infopane)
    @cursor = Cursor.new(0, 0, @map, @infopane)
  end

  # End turn of the active faction
  def next_faction_turn!
    @infopane.next_faction!

    # Have all factions played this turn so we are now back at the first one?
    if @infopane.faction == 1
      puts "=============\n" \
           "End of turn\n" \
           "=============\n"
      @infopane.next_turn!
    end

    # Announce game state at the start of turn of the next faction
    # TODO add change-of-active-faction screen
    puts "-----------\n" + @infopane.turnscore

    # Activate functions and reset moves for units of the next faction
    functionable_faction_units = @map.all_units.select { |uu|
      uu.faction == @infopane.faction and
      uu.function != FUNCNONE
    }
    functionable_faction_units.each { |uu| uu.function! }

    all_faction_units = @map.all_units.select { |uu|
      uu.faction == @infopane.faction
    }
    all_faction_units.each { |uu| uu.reset_moves!}

    # Win check: current faction has lost all units
    if !WinState.instance.faction and all_faction_units.empty?
      WinState.instance.set_victory!(
        3 - @infopane.faction, # TODO more factions?
        'Annihilation', @infopane.turn, @infopane.score
      )
    end

    # Lock the cursor to the first waiting unit
    @cursor.reset!
  end

  # Printout the controls
  def help
    puts "-----------\n" \
         "H: help, Esc: menu, Enter: info, " \
         ".: end your turn, J: switch freeroam\n" \
         "QWEADZXC or arrow keys: movement, K: previous unit, L: next unit\n" \
         "functions: S sentry, B build, N none\n" \
         "-----------\n"
  end
end
