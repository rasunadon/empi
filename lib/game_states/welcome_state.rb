require 'singleton'

require_relative './game_state'
require_relative './play_state'
require_relative './quit_state'
require_relative './set_state'
require_relative './start_new_state'
require_relative './win_state'

# Game state of main menu
class WelcomeState < GameState
  include Singleton

  attr_accessor :version

  # What to do just after state gets activated
  #def after_start
  #end

  # What to do just before state gets deactivated
  #def before_end
  #end

  # Process given button
  def update(button)
    case(button)
    when Gosu::KB_ESCAPE then
      # If there is an active map go either to it or to its win screen
      if PlayState.instance.map
        unless WinState.instance.faction
          GameState.switch!(PlayState.instance)
        else
          GameState.switch!(WinState.instance)
        end
      end
    when Gosu::KB_1, Gosu::KB_2, Gosu::KB_3 then
      PlayState.instance.desired_new_map = {
         Gosu::KB_1 => 'm01',
         Gosu::KB_2 => 'm02',
         Gosu::KB_3 => 'm03'
      }[button]
      # If there is an active map confirm starting of a new one
      if PlayState.instance.map
        GameState.switch!(StartNewState.instance)
      else
        GameState.switch!(PlayState.instance)
      end
    when Gosu::KB_9, Gosu::KB_S then
      GameState.switch!(SetState.instance)
    when Gosu::KB_0, Gosu::KB_Q then
      GameState.switch!(QuitState.instance)
    end
  end

  def draw
    header_text = '
        /--  ///  /-/  -/-
        /--  |||  /-/   |
        /--  |||  |    -/-'

    # Is there active map? Is it won?
    if PlayState.instance.map
      unless WinState.instance.faction
        resume_text = "
        Esc – resume"
      else
        resume_text = "
        Esc – return to victory screen"
      end
    else
      resume_text = "\n"
    end

    options_text = "
        1 – start new: Map 01
        2 – start new: Map 02
        3 – start new: Map 03\n
        9, S – settings
        0, Q – quit"

    version_text = "
        version #{self.version}"

    menu = Gosu::Image.from_text(
      header_text + "\n\n\n" +
      resume_text + "\n" +
      options_text + "\n\n\n\n\n\n\n\n" +
      version_text, LINE_HEIGHT)
    menu.draw((3*TILESIZE) + XTEXT, (2*TILESIZE) + YTEXT, ZTEXT)
  end
end
