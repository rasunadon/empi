# Managing of active game states
class GameState
  # Switch active state to next one
  def self.switch!(next_state)
    #puts 'DEBUG: switching to state ' + next_state.to_s

    unless $window.state.nil?
      $window.state.before_end
    end
    $window.state = next_state
    $window.state.after_start
  end

  # What to do just after state gets activated
  def after_start
  end

  # What to do just before state gets deactivated
  def before_end
  end

  def update(button)
  end

  def draw
  end
end
