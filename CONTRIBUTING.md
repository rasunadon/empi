/--  ///  /-/  -/-
/--  |||  /-/   |
/--  |||  |    -/-

# Contributing
Feedback is welcomed, especially in the form of [new issues or comments to
existing issues at Gitlab](https://gitlab.com/rasunadon/empi/-/issues).
Alternatively, there is an email adress in README. Or you may find me at
[Gosu Discord server](https://discord.com/invite/gTaHxdm).

GitLab is used as both archive of code and bugtracker. Ideally all but the
smallest changes to the code should have issue created first. The rest of this
file is explaining which gitting standards I am trying to use here (partially
so that I do not forget them myself) so please adhere to them when contributing
with issues or code. As a reminder, the license used here is CC-BY-SA 3.0.


## Structure of issue labels
Issues are tagged by labels to specify what type, area and wishlist they belong
to. There are three main groups of labels:

- type: whether issue is something new or something that is to be changed
- area: which parts of the project or the game itself is given issue about
- wishlist: whether given issue belongs to some wishlist

Issues typically have one type label, up to three area labels and none or
one wishlist label. New wishlist labels are being added as new wishlists are
created (_Wish I_, _Wish II_...). Issues planned to be closed before the next
version is deployed are temporarily marked with _In progress_ label.

Type labels
- _Feature_: desired new things that are to be added
    - such issues often need to be split into multiple smaller issues so
    that the desired goal is reachable
    - details of the final state can be unclear before that splitting occurs
- _Technical debt_: expected things that are to be changed
    - such issues often have a relatively clear path to the desired state
- _Bug_: unexpected things that are to be changed
    - such issues often need more testing to find out the main culprit
- _Removal_: undesired old things that are to be deleted
    - such issues are currently hardly used

Area labels
- _AI_ (_Artificial Intelligence_): robotic players and their strategies
- _Code style_: style of code, coding standards and environment
- _Docs_ (_Documentation_): documentation and in-game help
- _Game mechanics_: in-world mechanics and behaviour
- _Lore_: in-world terminology and story
- _Multiplayer_: support for multiple players, especially connecting from afar
- _UI_ (_User Interface_): game interface, input methods and media


## Format of commit messages
Once the development part on the feature branch is done the commits are to be
squashed before being rebased onto master so that it can be fast-forwarded to
include the new update. Each final commit is usually addressing one issue. The
structure of commit message of this final commit should include:

Summary
- type of commit, either docs or type of given issue (see the previous section)
  - docs: _Documentation_ area issues
  - feat: something got added
  - refact: something was updated
  - fix: something broken was repaired
  - del: something got removed
- "#XX", where XX is id of the main related issue
- colon
- summary of the main included change

Description
- "resolves #XX" where XX is id of to-be-closed issue (comma separated list)
- semicolon
- summary of other included changes (comma separated list)

Most commits have both summary and description filled though some may be lacking
"resolves" part as their related issue is processed over multiple commits. Some
commits can be completely without related issues, they still keep the same
commit message structure, just without naming any issue ids. Each to-be-closed
issue needs its own "resolves" part so as to be properly closed once the update
is deployed to master.


## Deployment process
To deploy Empi once work on given version is finished:
1. push closing commit `deploying` of `docs` type with only these changes:
    - CHANGELOG.md
        - remove unused subsections from `Not released yet`
        - add version name, tag link and release date to it
        - prepare new `Not released yet` header with its four subheaders
        in front of the old one
        - check spacing: one empty line between subsections in given version,
        two empty lines between sections
    - empi.rb
        - remove `dev` suffix from the version name variable
    - empi.gemspec
        - update version name
        - update release date
2. create somewhere a new gem folder (i.e. `v0.22`) out of `empi.gemspec` and
folder `lib` which includes copy of all Empi files
    ```
    v0.22
    │   empi.gemspec
    │
    └───lib
        │   CHANGELOG.md
        │   CONTRIBUTING.md
        │   empi.gemspec
        │   empi.rb
        │   README.md
        │
        └───data...
        └───lib...
        └───media...
        └───save...
    ```
3. open Terminal, navigate to the gem folder and build a gem file
(`gem build empi.gemspec` creates `empi-0.22.gem`)
4. install the new version (`sudo gem install empi-0.22.gem`)
5. check installed versions (`gem list | grep 'empi'`)
6. test the new version with small runfile `empitest.rb` created somewhere else
(`ruby empitest.rb` in its folder):

          gem 'empi', '=0.22.3' # select desired version
          require 'empi'

7. in case of more build versions mark them with subversion number
(both in `empi.rb` and gemspec file)
8. remember to git all last minute bugfixes and at least the latest version of
gemspec file
9. add new tag (`v22.3`) to the latest commit and push it to GitLab
10. add short summary of main changes to this tag in Gitlab, creating
a release out of it
11. update due date of current milestone to current day and close it
12. assign current milestone (`v22`) to the new release (milestones use just
version numbers, not subversion ones)
13. upload the final build version of the Empi gem to Rubygems.org
(`gem push empi-0.22.3.gem` in the folder with the gem)


## Post-deployment process
These steps are here so that all steps I need to not forget are at one place:
1. update the name of thread at bay12forums to include the newest version name
2. write devlog there ideally within the first hour since the gem upload
3. collect stats of Empi Impact (1 hour after upload and in nearby hours)
4. change the version name variable in `empi.rb` to the next version, add `dev`
 suffix again (`docs: new version` commit)
5. create new milestone (`v23`) with start day set to current day
6. assign few issues to it
