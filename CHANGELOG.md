/--  ///  /-/  -/-
/--  |||  /-/   |
/--  |||  |    -/-

# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Not released yet
### Added
- #115: screen with confirmation of starting a new scenario
- #126: saving and loading of settings

### Changed
- #63: game window as a separate class
- #132: transported units have explicit link to their transporter
- #127: ticks of axes centered to the respective side of a tile to stop
  overlaying in the corners
- #95: line height as just one static variable
- #134: all button static variables use SCREAMING_SNAKE_CASE style

### Fixed
- #132: units leaving their transporters are properly removed from cargo of that
  transporter instead of cargo of map unit at given coords

### Removed


## [v25](../../tags/v25) – 2021-09-13
### Added
- #83: skip to next or previous unit waiting for commands while in locked mode
- #14: settings screen
- #4: victory screen
- #4: Annihilation victory type (enemy has lost all units)
- #29: cycle to next or previous unit at current tile while in freeroam mode
- #79: version name in title screen

### Changed
- #14: all four axes can be enabled separately
- #123: transported units are recursively put behind their transporter in the
  lists of units (both those waiting for commands and those at given tile)
  instead of all of them being in the end of those lists
- #29: commands can be given to transported units also in freeroam mode,
  via cycling to them

### Fixed
- #128: transporter getting loaded into another transporter (ship with army
  into town) now updates position of its cargo
- #123: cursor in locked mode no more skips units transported in a transporter
  which is itself transported (armies in ship in town)
- #72: details about cargo of transported units can be found via cycling to
  them in freeroam mode


## [v24](../../tags/v24) – 2020-10-22
### Added
- #85: axes
- #61, #62, #120: CONTRIBUTING file with several gitting standards

### Changed
- #84: trying to show unit info for empty tile leads to a printout
  explaining there is no unit to show that info of
- #77: build screen options renamed
- #118: captured towns first reset to their default project (same as starting
  one, typically Army), only then build screen is opened
- #76: inspiration in README instead of only in gemspec file
- #76: game description says Empi is actually a hotseat multiplayer game

### Fixed
- #51: trying to change unit function while in freeroam mode also shows
  (possibly changed) unit info
- #118: towns do not crash because there was no project selected on capture

### Removed
- #119: support in build screen for towns without project is not needed
  as towns can no more be left without a project


## [v23](../../tags/v23) – 2020-08-25
### Added
- #12: title screen, both as the first welcome screen and ingame menu
- #80: active map can be restarted
- #8: all three maps can be selected and played instead of just m02
- #49: freeroam marker is shown when freeroam mode of cursor is active
- #87: faction counter in infopane
- #87: start of turn of faction announced as a printout
- #94: trying to set a function of empty tile leads to a printout
  explaining there is no unit to set that function to

### Changed
- #28: help is shown after each loading, not just after the first one
- #88: units of active faction function at the start of turn of that faction
  instead of units of all factions functioning together at the start of turn
- #7: factions are separated so each faction can give commands only to its own units

### Fixed
- #89: cursor again jumps to the last unit it was locked to when it is switched
  out of freeroam
- #91: unit info is printed out just once after switching out of freeroam mode
- #99: towns do not crash while trying to reset their functions


## [v22.3](../../tags/v22.3) – 2020-07-05
### Added
- #59: list of units in README
- #68: build info added to the list of units
- #66: change of build project can be cancelled
- #70: trying to set a function of neutral unit leads to a printout
  explaining those can't have functions set

### Changed
- #54: build progress is shown in build screen so that it is always findable
- #50: captured towns lose also their function in the capturing process so that
  it can't be disclosed to the new faction
- #73: selecting "cancel" option in build screen of town without build project
  behaves the same as if "none" was selected
- #58: transporters can be loaded into other transporters only when those have
  more cargo capacity (currently just ships in towns)

### Fixed
- #52: when just one part is lost singular form is used in the printout
- #69, #73: captured towns do not crash while trying to show details of
  their not-existing project
- #55: when capturing the info of attacker is shown just once


## [v21](../../tags/v21) – 2020-02-13
### Added
- #44: screen with confirmation of closing
- #18: selection of next build project
- #18: towns can build ships
- #48: gamespec file

### Changed
- #47: loading of units from file in Map
- game state stored as global variable


## [v20](../../tags/v20) – 2020-01-25
### Added
- GitLab started, with old versions rebuilt from local archive
- details of old versions in change log
- #40: install and gameplay hints in README
- #43: numpad keys can be used for movement, freeroam switching and getting unit info

### Changed
- info file split into CHANGELOG and README

### Fixed
- #35: writing of text in infopane has been updated to the new Gosu way
- #36: font warning was found to be an out-of-Empi issue
- #31: no-keys-pressed-yet state at the start no more leads to extra info printout

### Removed
- info file


## [v19](../../tags/v19) – 2019-12-04
### Added
- preliminary work done on setting of build projects
- towns say what their project and its progress are

### Changed
- #25: defenders of capturable units need to be dealt with before it can be captured
- cargo part of info shown even for empty transporters
- parts are first build and then checked so building process takes one less turn now
- captured towns have their build process reset
- reworded function parts of unit info

### Fixed
- #32: units which can't build will not only protest against such function but also not set it
- #34: destroyed transporters first destroy their cargo so that its points get counted
- #30: transported units now function themselves instead of their transporters

### Removed
- the old screenshot and new printout one got deleted to lower the size of gem


## [v18](../../tags/v18) – 2019-11-24
### Added
- End of turn marker
- End turn and End game keys in ingame help
- licensing/contact info was added also to README and not just to gem specs
- moving of cursor stores the current unit so it can possibly get moved (again)
- new screenshots of both game windows

### Changed
- help is now listing all keys
- help shown after the first load of new map
- units use all left moves when loaded, unless they are loaded into towns
- even units with 0 moves left show their moves in their info

### Fixed
- #22: transported units show in infopane their info instead of info of their transporters


## [v17](../../tags/v17) – 2016-04-20
### Added
- transport ships

### Changed
- selected map: m02
- #21: capturing can be done only by armies
- #5: only towns can be captured, other units do battle

### Fixed
- moving transporters don't leave their cargo behind
- #20: transported units show in printouts their info instead of info of their transporters


## [v16](../../tags/v16) – 2016-04-15
### Added
- first gem published
- #3: towns build armies
- units can transport other units
- units on sentry duty say so when functioning

### Changed
- neutral units can't have functions
- links to media and save files use full path
- newly built units are stored in given town instead of map


## [v15](../../tags/v15) – 2016-04-04
### Added
- checks of loaded terrain tiles and units

### Changed
- selected map: m03
- #11: units are now also loaded from file
- map stored as [row, column], not [xx, yy]

### Fixed
- destroyed units are properly dying


## [v14](../../tags/v14) – 2016-03-28
### Changed
- #11: terrain tiles are loaded from file


## [v13](../../tags/v13) – 2016-03-27
### Changed
- classes separated to their own files


## [v12](../../tags/v12) – 2016-03-27
### Changed
- unit function as a separate class


## [v11](../../tags/v11) – 2016-02-14
### Added
- #2: units can be also destroyed and not just captured

### Changed
- score renamed to infopane
- functions as static variables


## [v10](../../tags/v10) – 2016-01-29
### Added
- functions none and sentry

### Changed
- eight-directional movement instead of just four-directional one


## [v9](../../tags/v9) – 2016-01-29
### Added
- updating of score

### Changed
- units move and capture properly


## [v8](../../tags/v8) – 2016-01-29
### Added
- info about unit on given tile
- freeroaming cursor
- ingame help listing controls


## [v7](../../tags/v7) – 2016-01-29
### Added
- info about unit and turn

### Changed
- army and ship as separate classes


## [v6](../../tags/v6) – 2016-01-29
### Added
- cursor


## [v5](../../tags/v5) – 2016-01-29
### Added
- towns
- factions
- capturing


## [v4](../../tags/v4) – 2016-01-25
### Added
- stats of unit
- abilities of unit
- terrain checking


## [v3](../../tags/v3) – 2016-01-24
### Changed
- ground and sea instead of background


## [v2](../../tags/v2) – 2016-01-24
### Changed
- update on key press, not on hold


## [v1](../../tags/v1) – 2016-01-24
### Added
- keyboard input and output
- movement
