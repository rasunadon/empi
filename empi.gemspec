require 'rake'

Gem::Specification.new do |spec|
  spec.name        = 'empi'
  spec.version     = '0.25'
  spec.date        = '2021-09-13'
  spec.license     = 'CC-BY-SA-3.0'

  spec.summary = "A turn-based wargame"
  spec.description = "Empi: Ruby Edition is a turn-based wargame with hotseat
  multiplayer, currently in alpha development state. The game is inspired by
  Classic Empire, originally from 1977."
  spec.files       = Rake::FileList["lib/lib/**/*",
                              "lib/media/*.png",
                              "lib/save/*.esf",
                              "lib/empi.rb"]

  spec.author      = 'Detros'
  spec.homepage    = 'http://www.bay12forums.com/smf/index.php?topic=157538'
  spec.email       = 'rasunadon@seznam.cz'
  spec.metadata = {
    "source_code_uri"   => "https://gitlab.com/rasunadon/empi",
    "bug_tracker_uri"   => "https://gitlab.com/rasunadon/empi/issues",
    "documentation_uri" => "https://gitlab.com/rasunadon/empi/blob/master/README.md",
    "changelog_uri"     => "https://gitlab.com/rasunadon/empi/blob/master/CHANGELOG.md",
    "homepage_uri"      => "http://www.bay12forums.com/smf/index.php?topic=157538",
  }

  spec.platform    = Gem::Platform::RUBY
  spec.add_runtime_dependency 'gosu', '~> 0.9'
  spec.add_development_dependency 'rake', '~> 10.0'
end
